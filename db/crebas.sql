
-- Crear tabla integrantes
CREATE TABLE integrantes (
  matricula TEXT PRIMARY KEY,
  nombre TEXT,
  apellido TEXT,
  borralog BOOLEAN DEFAULT FALSE
);

-- Crear tabla tipomedia
CREATE TABLE tipomedia (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre TEXT,
  borralog BOOLEAN DEFAULT FALSE
);

-- Crear tabla media
CREATE TABLE media (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre TEXT,
  src TEXT,
  url TEXT,
  titulo TEXT,
  matricula TEXT,
  borralog BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (matricula) REFERENCES integrantes(matricula),
  FOREIGN KEY (id) REFERENCES tipomedia(id)
);

-- Crear tabla home
CREATE TABLE home (
  nombre VARCHAR(50),
  titulo VARCHAR(50),
  src VARCHAR(100),
  alt VARCHAR(50)
);