const express = require('express');

const hbs = require("hbs");
const app = express();
require("dotenv").config({ path: '.env' });



//Import archivo de Rutas
const router = require("./routes/public");
app.use("/",router);

//conf para app express
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

hbs.registerPartials(__dirname + "/views/partials");

const puerto = process.env.PORT || 3000;
    

app.listen(puerto, () => {
    console.log("El servidor se está ejecutando en en el puerto " + puerto );
});







